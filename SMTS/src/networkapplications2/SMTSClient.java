package networkapplications2;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.Socket;
import java.io.*;
import java.nio.CharBuffer;
import java.util.stream.Stream;

public class SMTSClient {
    public static void main(String[] args) throws Exception{
        String sentence;
        String modifiedSentence;
        int portNumber = Integer.parseInt(args[0]);

        BufferedReader inFromUser = new BufferedReader(new InputStreamReader(System.in));
        Socket clientSocket = new Socket("localhost", portNumber);

        DataOutputStream outToServer = new DataOutputStream(clientSocket.getOutputStream());
        BufferedReader inFromServer = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
        if(args.length>2)
            sentence = args[1];
        else
            sentence = inFromUser.readLine();

        outToServer.writeBytes(sentence +'\n');
        modifiedSentence = inFromServer.readLine();
        while (modifiedSentence!=null) {
            System.out.println(modifiedSentence);
            modifiedSentence = inFromServer.readLine();
        }
        clientSocket.close();
    }

}
