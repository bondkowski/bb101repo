package networkapplications2;


import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.*;


public class ScalableMultiThreadServer {
    public boolean isHELLO(String s){
        if(s.length()<5)
            return false;
        else {
            String word = s.substring(0, 5);
            return word.equals("HELLO");
        }
    }
    public static void main(String[] args) throws IOException {

        if(args.length != 1) {
            System.err.println("Incorrect arguments! Type TCP port number as only argument");
            System.exit(1);
        }
        ScalableMultiThreadServer server = new ScalableMultiThreadServer();
        ThreadPoolExecutor executorService = (ThreadPoolExecutor)Executors.newFixedThreadPool(10);
        int portNumber = Integer.parseInt(args[0]);
        String initSentence;
        ServerSocket mainSocket = new ServerSocket(portNumber);



        while (true) {
            int queueSize = executorService.getQueue().size();
            Socket listeningSocket = mainSocket.accept();
            BufferedReader initMessageFromClient = new BufferedReader(new InputStreamReader(listeningSocket.getInputStream()));
            DataOutputStream responseToClient = new DataOutputStream(listeningSocket.getOutputStream());
            initSentence = initMessageFromClient.readLine();

            if(queueSize > 10) {
                responseToClient.writeBytes("Server is overloaded... Sorry!\n");
            }else if(server.isHELLO(initSentence)){
                responseToClient.writeBytes(initSentence +"\nIP: " + listeningSocket.getLocalAddress() + "\nPort: " + listeningSocket.getLocalPort() + "\nStudentID:13312345\n");
            }else if(initSentence.equals("KILL_SERVICE")){
                responseToClient.writeBytes("Remote service terminated\n");
                executorService.shutdownNow();
                mainSocket.close();
                System.exit(1);
            }else {
                executorService.submit(new ServiceThread(listeningSocket,initSentence));
            }
        }
    }
}
