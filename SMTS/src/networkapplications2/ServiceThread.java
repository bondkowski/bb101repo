package networkapplications2;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;


public class ServiceThread implements Runnable {
    private Socket connectionSocket = null;
    private String clientSentence;

    public ServiceThread(Socket socket, String sentence){
        new Thread("ClientThread");
        this.connectionSocket = socket;
        this.clientSentence = sentence;
    }

    public void run(){
        try{
            DataOutputStream outToClient = new DataOutputStream(connectionSocket.getOutputStream());
            System.out.println("Received from client: " + clientSentence);
            String capitalizedSentence = clientSentence.toUpperCase() + '\n';
            Thread.sleep(1000);
            outToClient.writeBytes(capitalizedSentence);
        }catch (IOException e){
            System.err.println("Can not listen to the socket:  " + connectionSocket.getLocalPort());
            e.getMessage();
            e.printStackTrace();
        }catch (InterruptedException e){
            System.err.println("Something went wrong:  " + connectionSocket.getLocalPort());
            e.getMessage();
            e.printStackTrace();
        }
    }
}
